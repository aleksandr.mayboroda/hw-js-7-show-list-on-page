// перебор элементов массива
function showListElemets(listArray,parent = document.body)
{
    if(Array.isArray(listArray))
    {
        // 1. Отображаем рекурсией элементы и списки
        addElemToParent(parent,mapArray(listArray))

        // 2. Добавляем блок со счетчиком
        addElemToParent(parent,`<p class="counter-block">Страница будет очищена через <span class="counter">3</span> сек</p>`,'beforebegin')

        // 3. Отсчет счетчика до 0 и замена содержимого document.body
        counterToZero()
    }
  
}

// map массива, рекурсия
function mapArray(expleArr) {
    return expleArr.map(listElem => {
        if(Array.isArray(listElem))
        {
           return `<li><ul class="sub-list">${mapArray(listElem)}</ul></li>`
        }
        else
        {
            return `<li>${listElem}</li>`
        }
    }).join('')
}

// добавление на страницу
function addElemToParent(parent,elems,where='beforeend')
{
    parent.insertAdjacentHTML(where,elems)
}

function counterToZero() {
    let counter = document.querySelector('.counter')

    if(counter)
    {
        let i = counter.innerText
        setInterval(function() {
            if(i > 0)
            {
                if(i === 1)
                {
                    counter.style.color = 'red'
                }
                counter.innerHTML = i--
            }
            else
            {
                clearPage() //очистка страницы
            }
        },1000)
    }
}

// очистка тела, вставляю свой текст
function clearPage() {
   document.body.innerHTML = '<div class="container"><h1 class="page-title">Фсьо!</h1></div>'
}

//запуск по кнопке
document.querySelector('.task-start').addEventListener('click', () => {
    // showListElemets(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.querySelector('.result'))
    showListElemets(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper",["Zaporizhya", "Berdyansk"]], document.querySelector('.result'))
    // showListElemets(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])
})

